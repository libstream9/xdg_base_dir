#ifndef STREAM9_XDG_BASE_DIR_HPP
#define STREAM9_XDG_BASE_DIR_HPP

#include <stream9/array.hpp>
#include <stream9/string.hpp>

/*
 * based on XDG Base Directory Specification version 0.7
 * https://specifications.freedesktop.org/basedir-spec/basedir-spec-0.7.html
 */
namespace stream9::xdg {

/**
 * @return a absolute path to base directory to which user-specific data
 *         files should be written. The path is defined by environment
 *         variable XDG_DATA_HOME.
 * @return default path ($HOME/.local/share) if XDG_DATA_HOME isn't set or is
 *         empty or contains relative path.
 */
string data_home();

/**
 * @return a absolute path to base directory to which user-specific configuration
 *         files should be written. The path is defined by environment
 *         variable XDG_CONFIG_HOME.
 * @return default path ($HOME/.config) if XDG_CONFIG_HOME isn't set or is
 *         empty or contains relative path.
 */
string config_home();

/**
 * @return a absolute path to base directory to which user-specific non-essential
 *         (cached) data should be written. The path is defined by environment
 *         variable XDG_CACHE_HOME.
 * @return default path ($HOME/.cache) if XDG_CACHE_HOME isn't set or empty or
 *         contains relative path.
 */
string cache_home();

/**
 * @return (optional) a absolute path to base directory to which user-specific
 *         runtime files and other file objects should be placed. The path is
 *         defined by environment variable XDG_RUNTIME_DIR.
 * @return default path (/run/user/$UID) if XDG_RUNTIME_DIR isn't set or is
 *         empty or contains relative path.
 */
string runtime_dir();

/**
 *  @return a list of absolute paths to preference ordered base directories
 *          from which data file should be searched. Paths are defined by
 *          environment variable XDG_DATA_DIRS.
 *  @return default paths (/usr/local/share/, /usr/share/) if XDG_DATA_DIRS
 *          isn't set or is empty or all of its values are relative path.
 */
array<string> data_dirs();

/**
 *  @return a list of absolute paths to preference ordered base directories
 *          from which configuration files should be searched. Paths are
 *          defined by environment variable XDG_CONFIG_DIRS.
 */
array<string> config_dirs();

} // namespace stream9::xdg

#endif // STREAM9_XDG_BASE_DIR_HPP
