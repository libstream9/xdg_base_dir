#include <stream9/xdg/base_dir.hpp>

#include <stream9/emplace_back.hpp>
#include <stream9/log.hpp>
#include <stream9/path/absolute.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/path/home_directory.hpp>
#include <stream9/push_back.hpp>
#include <stream9/strings/query/empty.hpp>
#include <stream9/strings/query/split.hpp>
#include <stream9/to_string.hpp>

#include <stdlib.h>

namespace stream9::xdg {

namespace st9 = stream9;

static bool
is_absolute(string_view p, string_view const env)
{
    if (path::is_absolute(p)) return true;

    log::warn() << "environment variable" << env
                << "contains a relative path:" << p;

    return false;
}

string
data_home()
{
    using path::operator/;

    try {
        auto* env = ::getenv("XDG_DATA_HOME");
        if (env && !str::empty(env)) {
            string p { env };

            if (is_absolute(p, "XDG_DATA_HOME")) return p;
        }

        return path::home_directory() / ".local/share";
    }
    catch (...) {
        rethrow_error();
    }
}

string
config_home()
{
    using stream9::path::operator/;

    try {
        auto* env = ::getenv("XDG_CONFIG_HOME");
        if (env && !str::empty(env)) {
            string p { env };

            if (is_absolute(p, "XDG_CONFIG_HOME")) return p;
        }

        return path::home_directory() / ".config";
    }
    catch (...) {
        rethrow_error();
    }
}

string
cache_home()
{
    using stream9::path::operator/;

    try {
        auto* env = ::getenv("XDG_CACHE_HOME");
        if (env && !str::empty(env)) {
            string p { env };

            if (is_absolute(p, "XDG_CACHE_HOME")) return p;
        }

        return path::home_directory() / ".cache";
    }
    catch (...) {
        rethrow_error();
    }
}

string
runtime_dir()
{
    using stream9::path::operator/;

    try {
        auto* env = ::getenv("XDG_RUNTIME_DIR");
        if (env && !str::empty(env)) {
            string p { env };

            if (is_absolute(p, "XDG_RUNTIME_DIR")) return p;
        }

        auto uid = ::getuid();

        return "/run/user" / stream9::to_string(uid);
    }
    catch (...) {
        rethrow_error();
    }
}

array<string>
data_dirs()
{
    try {
        array<string> result;

        auto* env = ::getenv("XDG_DATA_DIRS");
        if (env && !str::empty(env)) {
            for (auto const& rng: str::split(env, ':')) {
                string p { rng };

                if (is_absolute(p, "XDG_DATA_DIRS")) {
                    st9::push_back(result, std::move(p));
                }
            }
        }

        if (result.empty()) {
            st9::emplace_back(result, "/usr/local/share/");
            st9::emplace_back(result, "/usr/share/");
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

array<string>
config_dirs()
{
    using std::ranges::empty;

    try {
        array<string> result;

        auto* env = ::getenv("XDG_CONFIG_DIRS");
        if (env && !str::empty(env)) {
            for (auto const& rng: str::split(env, ':')) {
                string p { rng };

                if (is_absolute(p, "XDG_CONFIG_DIRS")) {
                    st9::push_back(result, std::move(p));
                }
            }
        }

        if (empty(result)) {
            st9::emplace_back(result, "/etc/xdg");
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg
